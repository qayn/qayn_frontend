import {Redirect, Route, RouteProps} from "react-router";
import {isLogged} from "../api/auth_api";
import React from "react";

export default function UnauthOnlyRoute(props: RouteProps): JSX.Element {
    const {component: Component, ...rest} = props;
    return (
        <Route
            {...rest}
            render={(routeProps): React.ReactNode =>
                !isLogged() ? (<Component {...routeProps} />) : (
                    <Redirect
                        to={{
                            pathname: "/home",
                            state: {from: routeProps.location},
                        }}
                    />
                )
            }
        />
    );
}