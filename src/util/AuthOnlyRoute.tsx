import {Redirect, Route, RouteProps} from "react-router";
import React from "react";
import {isLogged} from "../api/auth_api";

export default function AuthOnlyRoute(props: RouteProps): JSX.Element {
    const {component: Component, ...rest} = props;
    return (
        <Route
            {...rest}
            render={(routeProps): React.ReactNode =>
                isLogged() ? (<Component {...routeProps} />) : (
                    <Redirect
                        to={{
                            pathname: "/sign-in",
                            state: {from: routeProps.location},
                        }}
                    />
                )
            }
        />
    );
}