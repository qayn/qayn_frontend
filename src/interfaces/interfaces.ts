export interface ApiResponse<T> {
    code: number;
    response: T;
}

export interface AlertMessage {
    success: boolean;
    message: string;
}

export interface ErrorBody {
    message: string;
}

export interface Quote {
    id: number;
    text: string;
    userRating: string;
    mainSource: Source;
    otherSources: Source[];
}

export interface Source {
    id: number;
    name: string;
    type: string;
}

export interface User {
    login: string;
    email: string;
}

export interface PasswordChangeRequest {
    oldPassword: string;
    newPassword: string;
}

export interface ProfileUpdateRequest {
    login: string;
    email: string;
}

export interface AuthenticationResponseBody{
    token: string;
}

export interface AuthenticationRequest {
    login: string;
    password: string;
}

export interface RegistrationRequest {
    login: string;
    email: string;
    password: string;
}

export type GetQuoteResponse = Entity<Quote> | ErrorBody;
export type UserResponse = Entity<User> | ErrorBody;
export type GetQuotesResponse = Entity<Quote[]> | ErrorBody;
export type GetSourceResponse = Entity<Source> | ErrorBody;
export type PostResult = string | ErrorBody;
export type Entity<T> = T | string;
export type Confirmation = string | true;