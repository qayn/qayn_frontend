import {
    ApiResponse,
    Quote,
    Entity,
    PostResult,
    RegistrationRequest,
    AuthenticationRequest,
    User, PasswordChangeRequest, ProfileUpdateRequest, Source, ErrorBody, GetQuoteResponse, UserResponse, GetQuotesResponse, GetSourceResponse
} from "../interfaces/interfaces";
import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from "axios";
import Cookies from "js-cookie";

export interface ApiClient {
    register(body: RegistrationRequest): Promise<ApiResponse<PostResult>>;

    login(body: AuthenticationRequest): Promise<ApiResponse<PostResult>>;

    rateQuote(id: number, like: boolean): Promise<ApiResponse<PostResult>>;

    removeQuoteRating(id: number): Promise<ApiResponse<PostResult>>;

    randomQuote(): Promise<ApiResponse<GetQuoteResponse>>;

    recommendCurrent(): Promise<ApiResponse<GetQuoteResponse>>;

    recommendMoveBackward(): Promise<ApiResponse<PostResult>>;

    recommendMoveForward(): Promise<ApiResponse<PostResult>>;

    profile(): Promise<ApiResponse<UserResponse>>;

    changePassword(body: PasswordChangeRequest): Promise<ApiResponse<PostResult>>;

    editProfile(body: ProfileUpdateRequest): Promise<ApiResponse<PostResult>>;

    myCollection(): Promise<ApiResponse<GetQuotesResponse>>;

    getSource(id: number): Promise<ApiResponse<GetSourceResponse>>;

    quotesFromSource(id: number): Promise<ApiResponse<GetQuotesResponse>>;

    searchQuotes(searchString: string): Promise<ApiResponse<GetQuotesResponse>>;
}

export function handleConfirmation(result: ApiResponse<PostResult>, correctCode: number) {
    const {response, code} = result;

    if (code === correctCode) {
        return true;
    } else {
        const {message} = response as ErrorBody;
        return message;
    }
}

function confirmationHandler<T>(result: AxiosResponse<T>): ApiResponse<T> {
    const data = result.data as T;
    return {
        code: result.status,
        response: data
    } as ApiResponse<T>;
}

function errorHandler(error: AxiosError): ApiResponse<ErrorBody> {
    const result = error.response;
    if (result !== undefined) {
        return {
            code: result.status,
            response: {
                message: result.data.message,
            },
        } as ApiResponse<ErrorBody>;
    } else {
        return {
            code: parseInt(error.code),
            response: {
                message: error.message
            } 
        }
    }
}

function getAxiosConfig(): AxiosRequestConfig {
    const token = Cookies.get('token');
    return {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    } as AxiosRequestConfig;
}

class ApiClientImpl implements ApiClient {
    private static api: ApiClient;

    private constructor() {
    }

    public static getInstance(): ApiClient {
        ApiClientImpl.api = new ApiClientImpl();
        return ApiClientImpl.api;
    }

    async register(body: RegistrationRequest): Promise<ApiResponse<PostResult>> {
        return await axios.post<PostResult>(getURL() + `/register`, body)
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async login(body: AuthenticationRequest): Promise<ApiResponse<PostResult>> {
        return await axios.post<PostResult>(getURL() + `/login`, body)
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async rateQuote(id: number, like: boolean): Promise<ApiResponse<PostResult>> {
        return await axios.post<PostResult>(getURL() + `/rateQuote?quoteId=${id}&like=${like}`, {}, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async removeQuoteRating(id: number): Promise<ApiResponse<PostResult>> {
        return await axios.post<PostResult>(getURL() + `/rateQuote?quoteId=${id}`, {}, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async randomQuote(): Promise<ApiResponse<GetQuoteResponse>> {
        return await axios.get<Quote>(getURL() + '/randomQuote')
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async recommendCurrent(): Promise<ApiResponse<GetQuoteResponse>> {
        return await axios.get<Quote>(getURL() + '/recommend/get', getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async recommendMoveBackward(): Promise<ApiResponse<PostResult>> {
        return await axios.post<PostResult>(getURL() + '/recommend/moveBackward', getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async recommendMoveForward(): Promise<ApiResponse<PostResult>> {
        return await axios.post<PostResult>(getURL() + '/recommend/moveForward', getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async profile(): Promise<ApiResponse<UserResponse>> {
        return await axios.get<User>(getURL() + '/profile', getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async changePassword(body: PasswordChangeRequest): Promise<ApiResponse<PostResult>> {
        return await axios.post<PostResult>(getURL() + '/profile/changePassword', body, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async editProfile(body: ProfileUpdateRequest): Promise<ApiResponse<PostResult>> {
        return await axios.post<PostResult>(getURL() + '/profile/edit', body, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async myCollection(): Promise<ApiResponse<GetQuotesResponse>> {
        return await axios.get<Quote[]>(getURL() + '/myCollection', getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async getSource(id: number): Promise<ApiResponse<GetSourceResponse>> {
        return await axios.get<Source>(getURL() + `/source/${id}`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async quotesFromSource(id: number): Promise<ApiResponse<GetQuotesResponse>> {
        return await axios.get<Quote[]>(getURL() + `/quotesFromSource/${id}`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async searchQuotes(searchString: string): Promise<ApiResponse<GetQuotesResponse>>{
        return await axios.get<Quote[]>(getURL() + `/searchQuotes?query=${searchString}`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }
}

export default function api(): ApiClient {
    return ApiClientImpl.getInstance();
}

export function getURL(): string {
    if (process.env.NODE_ENV === "production") {
        return "http://104.197.96.199:4321/api";
    } else {
        return "https://virtserver.swaggerhub.com/lozhnikov/QAYN/1.0.0";
        //return "http://localhost:4321/api";
    }
}