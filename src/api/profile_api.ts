import {Confirmation, Entity, PasswordChangeRequest, PostResult, ProfileUpdateRequest, User} from "../interfaces/interfaces";
import api, {handleConfirmation} from "./api";

export async function profile(): Promise<Entity<User>> {
    return api().profile().then(result => {
        const {code, response} = result;
        if (code === 200) {
            return response as User;
        }
        return (response as string);
    })
}

export async function changePassword(body: PasswordChangeRequest): Promise<Confirmation> {
    return api().changePassword(body).then(result => handleConfirmation(result, 200));
}

export async function editProfile(body: ProfileUpdateRequest): Promise<Confirmation> {
    return api().editProfile(body).then(result => handleConfirmation(result, 200));
}