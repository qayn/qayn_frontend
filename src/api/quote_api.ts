import {Quote, Entity, PostResult, Confirmation} from "../interfaces/interfaces";
import api, {handleConfirmation} from "./api";

export async function rateQuote(id: number, like: boolean): Promise<Confirmation> {
    return api().rateQuote(id, like).then(result => handleConfirmation(result, 200));
}

export async function removeQuoteRating(id: number): Promise<Confirmation> {
    return api().removeQuoteRating(id).then(result => handleConfirmation(result, 200));
}

export async function randomQuote(): Promise<Entity<Quote>> {
    return api().randomQuote().then(result => {
        const {code, response} = result;
        if (code === 200) {
            return response as Quote;
        }
        return (response as string);
    })
}

export async function recommendCurrent(): Promise<Entity<Quote>> {
    return api().recommendCurrent().then(result => {
        const {code, response} = result;
        if (code === 200) {
            return response as Quote;
        }
        return (response as string);
    })
}

export async function recommendMoveBackward(): Promise<Confirmation> {
    return api().recommendMoveBackward().then(result => handleConfirmation(result, 200));
}

export async function recommendMoveForward(): Promise<Confirmation> {
    return api().recommendMoveForward().then(result => handleConfirmation(result, 200));
}