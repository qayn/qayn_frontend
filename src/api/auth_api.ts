import Cookies from "js-cookie";
import {PostResult, RegistrationRequest, AuthenticationRequest, AuthenticationResponseBody, AlertMessage, ErrorBody, Confirmation} from "../interfaces/interfaces";
import api, {handleConfirmation} from "./api";

const ACCESS_TOKEN_COOKIE = "token";

export function isLogged(): boolean {
    const jwt = Cookies.get(ACCESS_TOKEN_COOKIE);
    return typeof jwt != "undefined";
}

export function logout(): void {
    Cookies.remove(ACCESS_TOKEN_COOKIE);
}

export async function register(body: RegistrationRequest): Promise<Confirmation> {
    return api().register(body).then(result => handleConfirmation(result, 201));
}

export async function login(body: AuthenticationRequest): Promise<Confirmation> {
    return api().login(body).then((resp) => {
        const {response, code} = resp;
        if(code === 200){
            const {token} = response as unknown as AuthenticationResponseBody;
            Cookies.set(ACCESS_TOKEN_COOKIE, token, {expires: 3600});
            return true;
        } else {
            const {message} = response as ErrorBody;
            console.log(message);
            return message;
        }
    });
}