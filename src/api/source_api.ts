import {Entity, Quote, Source} from "../interfaces/interfaces";
import api from "./api";

export async function getSource(id: number): Promise<Entity<Source>> {
    return api().getSource(id).then(result => {
        const {code, response} = result;
        if (code === 200) {
            return response as Source;
        }
        return (response as string);
    })
}

export async function quotesFromSource(id: number): Promise<Entity<Quote[]>> {
    return api().quotesFromSource(id).then(result => {
        const {code, response} = result;
        if (code === 200) {
            return response as Quote[];
        }
        return (response as string);
    })
}