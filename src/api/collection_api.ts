import {Entity, Quote} from "../interfaces/interfaces";
import api from "./api";

export async function myCollection(): Promise<Entity<Quote[]>> {
    return api().myCollection().then(result => {
        const {code, response} = result;
        if (code === 200) {
            return response as Quote[];
        }
        return (response as string);
    })
}

export async function searchQuotes(searchString: string): Promise<Entity<Quote[]>>{
    return api().searchQuotes(searchString).then(result => {
        const {code, response} = result;
        if(code === 200){
            return response as Quote[];
        }
        return (response as string);
    })
}