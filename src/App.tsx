import React from 'react';
import './App.css';
import {Redirect, Switch} from "react-router-dom";
import UnauthOnlyRoute from "./util/UnauthOnlyRoute";
import AuthOnlyRoute from "./util/AuthOnlyRoute";
import SignIn from "./components/SignIn";
import SignUp from "./components/SignUp";
import Home from "./components/Home";

function App() {
    return (
        <>
            <Switch>
                <UnauthOnlyRoute path="/sign-in" component={SignIn} exact/>
                <UnauthOnlyRoute path="/sign-up" component={SignUp} exact/>
                <AuthOnlyRoute path="/home" component={Home}/>
                <Redirect from="/" to="/sign-in" exact/>
            </Switch>
        </>
    );
}

export default App;
