import {useEffect, useState} from "react";
import {Quote, AlertMessage} from "../interfaces/interfaces";
import {
    recommendCurrent,
    randomQuote,
    recommendMoveBackward,
    recommendMoveForward,
    rateQuote,
    removeQuoteRating
} from "../api/quote_api";
import React from "react";
import {Button, Col, Container, Figure, Row, Alert} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Recommendations(): JSX.Element {
    
    //alert part
    const [show, setShow] = useState<boolean>(false);
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [alertVariant, setAlertVariant] = useState<string>("");

    const enableAlert = (alert: AlertMessage): void => {
        if (alert !== undefined && alert.message !== undefined) {
            setAlertMsg(alert.message);
            setShow(true);
            alert.success ? setAlertVariant("success") : setAlertVariant("danger");
        }
    };
    //end of alert part
    
    const [loading, setLoading] = useState<boolean>(true);

    const [quote, setQuote] = useState<Quote>({
        id: -1,
        text: "",
        userRating: "none",
        mainSource: {id: -1, name: "", type: ""},
        otherSources: [
            {id: -1, name: "", type: ""}
        ]
    });

    const [rating, setRating] = useState<string>("none");

    const loadQuote = () => {
        randomQuote().then(result => {          //TODO: change to bestQuote later
            if (typeof result !== 'string') {
                setQuote(result);
                if (result.userRating === "none" || result.userRating === "like" || result.userRating === "dislike")
                    setRating(result.userRating)
            } else {
                const failAlert = {
                    success: false,
                    message: result as string,
                };
                enableAlert(failAlert);
            }
        })
    }

    useEffect(() => {
        if (loading) {
            setLoading(false);
            loadQuote();
        }
    }, [loading]);

    const handleMoveBackward = (event): void => {
        recommendMoveBackward().then(loadQuote);
    }

    const handleMoveForward = (event): void => {
        recommendMoveForward().then(loadQuote);
    }

    const handleRating = (newRating: string) => {
        setRating(newRating);                   //чтобы кнопка не мелькала, ставлю рейтинг сразу
        if (newRating === "none") {             //TODO: добавить проверку, которая снимет рейтинг при ошибке
            removeQuoteRating(quote.id).then(() => setRating("none"));
        } else {
            rateQuote(quote.id, newRating === "like").then(() => setRating(newRating));
        }
    }

    const handleLike = (event): void => {
        if (rating !== "like") {
            handleRating("like");
        } else {
            handleRating("none");
        }
    }

    const handleDislike = (event): void => {
        if (rating !== "dislike") {
            handleRating("dislike");
        } else {
            handleRating("none");
        }
    }

    return (<>
        <Alert
            variant={alertVariant}
            show={show}
            onClose={(): void => setShow(false)}
            dismissible 
        >
            <div className="p-1">{alertMsg}</div>
        </Alert>
        <Container className="h-100">
            <Row className="align-items-center h-100">
                <Col>
                    <Button variant="outline-*" size="lg" onClick={handleMoveBackward}>&lt;</Button>
                </Col>
                <Col xs={10} className="mx-auto">
                    <Row>
                        <Figure className="text-center">
                            <blockquote className="blockquote mb-4">
                                <p className="display-6">{quote.text}</p>
                            </blockquote>
                            <figcaption className="blockquote-footer" style={{fontSize: "1.5rem"}}>
                                <Link to={`home/source/${quote.mainSource.id}`}>
                                    {quote.mainSource.name}
                                </Link>
                            </figcaption>
                            <ul className="list-inline">
                                {quote.otherSources.map(source => {     //TODO: add links
                                    return (<li className="list-inline-item text-muted mx-3">
                                        <Link to={`home/source/${source.id}`}>
                                            {source.name}
                                        </Link>
                                    </li>)
                                })}
                            </ul>
                        </Figure>
                    </Row>
                    <Row className="text-center">
                        <Col>
                            <input type="checkbox" checked={rating === "like"} className="btn-check" id="btn-like"
                                   autoComplete="off"/>
                            <label className="btn btn-outline-secondary" htmlFor="btn-like"
                                   onClick={handleLike}>[Плюс]</label>
                        </Col>
                        <Col>
                            <input type="checkbox" checked={rating === "dislike"} className="btn-check" id="btn-dislike"
                                   autoComplete="off"/>
                            <label className="btn btn-outline-secondary" htmlFor="btn-dislike"
                                   onClick={handleDislike}>[Минус]</label>
                        </Col>
                    </Row>
                </Col>
                <Col>
                    <Button variant="outline-*" size="lg" onClick={handleMoveForward}>&gt;</Button>
                </Col>
            </Row>
        </Container>
    </>)
}