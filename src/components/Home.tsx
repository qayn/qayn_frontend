import {Switch, useHistory, useRouteMatch} from "react-router";
import {Navbar, Image, Button, Nav} from "react-bootstrap";
import AuthOnlyRoute from "../util/AuthOnlyRoute";
import image from "../pics/logo.png";
import React from "react";
import styles from "../css/home.module.css";
import Recommendations from "./Recommendations";
import {logout} from "../api/auth_api";
import Profile from "./Profile";
import Collection from "./Collection";
import SourceQuotes from "./SourceQuotes";
import Search from "./Search";

export default function Home(): JSX.Element {
    const {url} = useRouteMatch();

    const history = useHistory();

    const onLogoutClick = (event): void => {
        event.preventDefault();
        logout();
        history.push("/sign-in")
    };

    return <>
        
        <Navbar bg="light" expand="lg">
            <Navbar.Brand className={styles.navBrand} href={`${url}`}>
                <Image src={image}/>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse>
                <Nav.Link className={styles.navLink} href={`${url}`}>
                    Рекомендации
                </Nav.Link>
                <Nav.Link className={styles.navLink} href={`${url}/search`}>
                    Поиск
                </Nav.Link>
                <Nav.Link className={styles.navLink} href={`${url}/collection`}>
                    Коллекция
                </Nav.Link>
            </Navbar.Collapse>
            <Navbar.Collapse className="justify-content-end">
                <Nav.Link className={styles.navLink} href={`${url}/profile`}>
                    Профиль
                </Nav.Link>
                <Button className={styles.logoutButton} variant="secondary" onClick={onLogoutClick}>
                    Выйти
                </Button>
            </Navbar.Collapse>
        </Navbar>
        <Switch>
            <AuthOnlyRoute path={`${url}`} component={Recommendations} exact/>
            <AuthOnlyRoute path={`${url}/profile`} component={Profile} exact/>
            <AuthOnlyRoute path={`${url}/collection`} component={Collection} exact/>
            <AuthOnlyRoute path={`${url}/source/:sourceId`} component={SourceQuotes} exact/>
            <AuthOnlyRoute path={`${url}/search`} component={Search} exact/>
        </Switch>
    </>
}