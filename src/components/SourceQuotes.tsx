import React, {useEffect, useState} from "react";
import {Quote, Source, AlertMessage} from "../interfaces/interfaces";
import {Container} from "react-bootstrap";
import styles from "../css/collection.module.css";
import QuoteList from "./QuoteList";
import {getSource, quotesFromSource} from "../api/source_api";
import {useParams} from "react-router";

export default function SourceQuotes(): JSX.Element {
    
    //alert part
    const [show, setShow] = useState<boolean>(false);
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [alertVariant, setAlertVariant] = useState<string>("");

    const enableAlert = (alert: AlertMessage): void => {
        if (alert !== undefined && alert.message !== undefined) {
            setAlertMsg(alert.message);
            setShow(true);
            alert.success ? setAlertVariant("success") : setAlertVariant("danger");
        }
    };
    //end of alert part
    
    const {sourceId} = useParams<{ sourceId: string }>();

    const [loading, setLoading] = useState<boolean>(true);

    const [quotes, setQuotes] = useState<Quote[]>([]);

    const [source, setSource] = useState<Source>({
        id: 0,
        name: "",
        type: ""
    });

    useEffect(() => {
        if (loading) {
            setLoading(false);

            getSource(parseInt(sourceId)).then(result => {
                if (typeof result !== "string") {
                    setSource(result);
                } else {
                    const failAlert = {
                        success: false,
                        message: result as string,
                    };
                    enableAlert(failAlert);
                }
            })

            quotesFromSource(parseInt(sourceId)).then(result => {
                if (typeof result !== "string") {
                    setQuotes(result);
                } else {
                    const failAlert = {
                        success: false,
                        message: result as string,
                    };
                    enableAlert(failAlert);
                }
            });
        }
    }, [loading, sourceId]);

    return <Container className={styles.mainContainer}>
        <h1>{source.name}</h1>
        <label className="mb-5">{source.type}</label>
        <QuoteList quotes={quotes}/>
    </Container>
}