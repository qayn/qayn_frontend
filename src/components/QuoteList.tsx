import {Quote} from "../interfaces/interfaces";
import {Col, Container, Figure, ListGroup, Row} from "react-bootstrap";
import React, {useState} from "react";
import {rateQuote, removeQuoteRating} from "../api/quote_api";
import {Link} from "react-router-dom";

interface QuoteListProps {
    quotes: Quote[];
}

interface QuoteListItemProps {
    quote: Quote;
}

export default function QuoteList(props: QuoteListProps) {
    return <ListGroup>
        {props.quotes.map(quote => <QuoteListItem quote={quote}/>)}
    </ListGroup>
}

function QuoteListItem(props: QuoteListItemProps) {
    const quote = props.quote;

    const [rating, setRating] = useState<string>(quote.userRating);

    const handleRating = (newRating: string) => {
        setRating(newRating);                   //чтобы кнопка не мелькала, ставлю рейтинг сразу
        if (newRating === "none") {             //TODO: добавить проверку, которая снимет рейтинг при ошибке
            removeQuoteRating(quote.id).then(() => setRating("none"));
        } else {
            rateQuote(quote.id, newRating === "like").then(() => setRating(newRating));
        }
    }

    const handleLike = (event): void => {
        if (rating !== "like") {
            handleRating("like");
        } else {
            handleRating("none");
        }
    }

    const handleDislike = (event): void => {
        if (rating !== "dislike") {
            handleRating("dislike");
        } else {
            handleRating("none");
        }
    }

    return <Container className="mb-4">
        <Row>
            <Col>
                <Figure>
                    <blockquote className="blockquote mb-4">
                        <p>{quote.text}</p>
                    </blockquote>
                    <figcaption className="blockquote-footer" style={{fontSize: "1rem"}}>
                        <Link to={`/home/source/${quote.mainSource.id}`}>
                            {quote.mainSource.name}
                        </Link>
                    </figcaption>
                    <ul className="list-inline">
                        {quote.otherSources.map(source =>           //TODO: add links
                            <li className="list-inline-item text-muted">
                                <Link to={`/home/source/${source.id}`}>
                                    {source.name}
                                </Link>
                            </li>
                        )}
                    </ul>
                </Figure>
            </Col>
            <Col xs={1}>
                <input type="checkbox" checked={rating === "like"} className="btn-check" id={`btn-like-${quote.id}`}
                       autoComplete="off" defaultChecked/>
                <label className="btn btn-outline-secondary" htmlFor={`btn-like-${quote.id}`}
                       onClick={handleLike}>[Плюс]</label>
            </Col>
            <Col xs={1}>
                <input type="checkbox" checked={rating === "dislike"} className="btn-check"
                       id={`btn-dislike-${quote.id}`}
                       autoComplete="off" defaultChecked/>
                <label className="btn btn-outline-secondary" htmlFor={`btn-dislike-${quote.id}`}
                       onClick={handleDislike}>[Минус]</label>
            </Col>
        </Row>
        <hr/>
    </Container>
}