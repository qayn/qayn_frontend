import React, {useEffect, useState} from "react";
import {ProfileUpdateRequest, User, AlertMessage} from "../interfaces/interfaces";
import {changePassword, editProfile, profile} from "../api/profile_api";
import {Button, Col, Container, Dropdown, FormControl, InputGroup, Row, Alert} from "react-bootstrap";
import styles from "../css/profile.module.css";
import {logout} from "../api/auth_api";

interface PasswordChangeProps {
    old: string;
    new: string;
    newConfirmation: string;
}

export default function Profile(): JSX.Element {
    
    //alert part
    const [show, setShow] = useState<boolean>(false);
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [alertVariant, setAlertVariant] = useState<string>("");

    const enableAlert = (alert: AlertMessage): void => {
        if (alert !== undefined && alert.message !== undefined) {
            setAlertMsg(alert.message);
            setShow(true);
            alert.success ? setAlertVariant("success") : setAlertVariant("danger");
        }
    };
    //end of alert part
    
    const [loading, setLoading] = useState<boolean>(true);

    const [user, setUser] = useState<User>({
        login: "",
        email: ""
    });

    const [passwordChangeProps, setPasswordChangeProps] = useState<PasswordChangeProps>({
        old: "",
        new: "",
        newConfirmation: ""
    });

    const [profileUpdateProps, setProfileUpdateProps] = useState<ProfileUpdateRequest>({
        login: "",
        email: ""
    });

    const loadProfile = () => {
        profile().then(result => {
            if (typeof result !== 'string') {
                setUser(result);
                setProfileUpdateProps(result);  //should work as long as result includes required fields
            } else {
                const failAlert = {
                    success: false,
                    message: result as string,
                };
                enableAlert(failAlert);
            }
        })
    }

    useEffect(() => {
        if (loading) {
            setLoading(false);
            loadProfile();
        }
    }, [loadProfile, loading]);

    const onProfileUpdateFormChange = (event): void => {
        setProfileUpdateProps({...profileUpdateProps, [event.target.id]: event.target.value})
    }

    const onSubmitProfileUpdate = (): void => {
        editProfile(profileUpdateProps).then(result => {
            if (result === true) {
                const successAlert = {
                    success: true,
                    message: "Данные успешно изменены. Пожалуйста, выполните вход.",
                };
                enableAlert(successAlert);
                logout();
            } else {
                const failAlert = {
                    success: false,
                    message: result as string,
                };
                enableAlert(failAlert);
            }
        })
    }

    const onChangePasswordFormChange = (event): void => {
        setPasswordChangeProps({...passwordChangeProps, [event.target.id]: event.target.value})
    }

    const onSubmitChangePassword = (): void => {
        changePassword({
            oldPassword: passwordChangeProps.old,
            newPassword: passwordChangeProps.new
        }).then(result => {
            if (result === true) {
                const successAlert = {
                    success: true,
                    message: "Пароль успешно изменён. Пожалуйста, выполните вход.",
                };
                enableAlert(successAlert);
                logout();
            } else {
                const failAlert = {
                    success: false,
                    message: result as string,
                };
                enableAlert(failAlert);
            }
        });
    }

    return <div>

        <Alert
            variant={alertVariant}
            show={show}
            onClose={(): void => setShow(false)}
            dismissible 
        >
            <div className="p-1">{alertMsg}</div>
        </Alert>

        <Container className={styles.mainContainer}>
            <h1>Профиль</h1>
            <Container className={styles.fieldsContainer}>
                <Row>
                    <Col>Логин</Col>
                    <Col>{user.login}</Col>
                </Row>
                <Row>
                    <Col>Почта</Col>
                    <Col>{user.email}</Col>
                </Row>
            </Container>
            <Container className={styles.fieldsContainer}>
                <Row>
                    <Col>
                        <Dropdown className={styles.dropdown}>
                            <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                                Редактировать профиль
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <InputGroup className={`m-3 ${styles.inputDropdown}`}>
                                    <FormControl
                                        type="input"
                                        placeholder="Логин"
                                        id="login"
                                        onChange={onProfileUpdateFormChange}
                                        className={styles.formElement}
                                        value={profileUpdateProps.login}
                                    />
                                </InputGroup>
                                <InputGroup className={`m-3 ${styles.inputDropdown}`}>
                                    <FormControl
                                        type="input"
                                        placeholder="Почта"
                                        id="email"
                                        onChange={onProfileUpdateFormChange}
                                        className={styles.formElement}
                                        value={profileUpdateProps.email}
                                    />
                                </InputGroup>
                                <InputGroup className={`m-3 ${styles.inputDropdown}`}>
                                    <Button className={styles.formElement}
                                            onClick={onSubmitProfileUpdate}
                                            variant="outline-success"
                                            disabled={profileUpdateProps.login === "" || profileUpdateProps.email === ""}
                                    >
                                        Сохранить
                                    </Button>
                                </InputGroup>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Col>
                    <Col>
                        <Dropdown className={styles.dropdown}>
                            <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                                Изменить пароль
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <InputGroup className={`m-3 ${styles.inputDropdown}`}>
                                    <FormControl
                                        type="password"
                                        placeholder="Старый пароль"
                                        id="old"
                                        onChange={onChangePasswordFormChange}
                                        className={styles.formElement}
                                    />
                                </InputGroup>
                                <InputGroup className={`m-3 ${styles.inputDropdown}`}>
                                    <FormControl
                                        type="password"
                                        placeholder="Новый пароль"
                                        id="new"
                                        onChange={onChangePasswordFormChange}
                                        className={styles.formElement}
                                    />
                                </InputGroup>
                                <InputGroup className={`m-3 ${styles.inputDropdown}`}>
                                    <FormControl
                                        type="password"
                                        placeholder="Подтверждение пароля"
                                        id="newConfirmation"
                                        onChange={onChangePasswordFormChange}
                                        className={styles.formElement}
                                    />
                                </InputGroup>
                                <InputGroup className={`m-3 ${styles.inputDropdown}`}>
                                    <Button className={styles.formElement}
                                            onClick={onSubmitChangePassword}
                                            variant="outline-success"
                                            disabled={passwordChangeProps.old === "" || passwordChangeProps.new === "" ||
                                            passwordChangeProps.new !== passwordChangeProps.newConfirmation}
                                    >
                                        Сохранить
                                    </Button>
                                </InputGroup>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Col>
                </Row>
            </Container>
        </Container>
    </div>
}