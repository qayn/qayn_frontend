import {Quote, AlertMessage} from "../interfaces/interfaces";
import React, {useEffect, useState} from "react";
import {myCollection} from "../api/collection_api";
import QuoteList from "./QuoteList";
import styles from "../css/collection.module.css";
import {Container, Alert} from "react-bootstrap";

export default function Collection(): JSX.Element {
    
    //alert part
    const [show, setShow] = useState<boolean>(false);
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [alertVariant, setAlertVariant] = useState<string>("");

    const enableAlert = (alert: AlertMessage): void => {
        if (alert !== undefined && alert.message !== undefined) {
            setAlertMsg(alert.message);
            setShow(true);
            alert.success ? setAlertVariant("success") : setAlertVariant("danger");
        }
    };
    //end of alert part
    
    const [loading, setLoading] = useState<boolean>(true);

    const [quotes, setQuotes] = useState<Quote[]>([]);

    useEffect(() => {
        if (loading) {
            setLoading(false);
            myCollection().then(result => {
                if (typeof result !== "string") {
                    setQuotes(result);
                } else {
                    const failAlert = {
                        success: false,
                        message: result as string,
                    };
                    enableAlert(failAlert);
                }
            });
        }
    }, [loading]);

    return <Container className={styles.mainContainer}>
         <Alert
            variant={alertVariant}
            show={show}
            onClose={(): void => setShow(false)}
            dismissible 
        >
            <div className="p-1">{alertMsg}</div>
        </Alert>
        <h1 className="mb-5">Моя коллекция</h1>
        <QuoteList quotes={quotes}/>
    </Container>
}