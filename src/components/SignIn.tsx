import React, {FormEvent, useEffect, useState} from "react";
import {Button, Col, Container, Form, Row, Figure, Alert} from "react-bootstrap";
import styles from "../css/signin.module.css";
import {login} from "../api/auth_api";
import {randomQuote} from "../api/quote_api";

import {AlertMessage, AuthenticationRequest, Quote} from "../interfaces/interfaces";
import {useHistory} from "react-router";

export default function SignIn(): JSX.Element {

    //alert part
    const [show, setShow] = useState<boolean>(false);
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [alertVariant, setAlertVariant] = useState<string>("");

    const enableAlert = (alert: AlertMessage): void => {
        if (alert !== undefined && alert.message !== undefined) {
            setAlertMsg(alert.message);
            setShow(true);
            alert.success ? setAlertVariant("success") : setAlertVariant("danger");
        }
    };
    //end of alert part

    const [loading, setLoading] = useState<boolean>(true);

    //const [isRegistrationEnabled, setRegistrationEnabled] = useState<boolean>(true);
    const [credentials, setCredentials] = React.useState<AuthenticationRequest>({
        password: "",
        login: ""
    });

    const [quote, setQuote] = useState<Quote>({
        id: -1,
        text: "",
        userRating: "none",
        mainSource: {id: -1, name: "", type: ""},
        otherSources: [
            {id: -1, name: "", type: ""}
        ]
    });

    const history = useHistory();

    const onSubmit = async (event: FormEvent) => {
        event.preventDefault();
        login(credentials).then(result =>{
            if(result === true){
                history.push("/");
            } else {
                const failAlert = {
                    success: false,
                    message: result as string,
                };
                enableAlert(failAlert);
            }
        });
    };

    const gotoRegister = async (event) => {
        event.preventDefault();
        history.push("/sign-up");
    }

    const onFormChange = (event): void => {
        setCredentials({...credentials, [event.target.id]: event.target.value});
    };

    const loadQuote = () => {
        randomQuote().then(result => {
            if (typeof result !== 'string') {
                setQuote(result);
            } else {
                const failAlert = {
                    success: false,
                    message: result as string,
                };
                enableAlert(failAlert);
            }
        })
    }

    useEffect(() => {
        if (loading) {
            setLoading(false);
            loadQuote();
        }
    }, [loading]);

    return (
        <>
            <Alert
                variant={alertVariant}
                show={show}
                onClose={(): void => setShow(false)}
                dismissible 
            >
                <div className="p-1">{alertMsg}</div>
            </Alert>
            <Container className={styles.container}>
                <Row className={styles.formRow}>
                    <Col md="auto">
                        <Form className={styles.form} onSubmit={onSubmit} onChange={onFormChange}>
                            <h3>Quote is All You Need</h3>

                            <Form.Group className={styles.enterInput} controlId="login">
                                <Form.Label>Логин</Form.Label>
                                <Form.Control placeholder="Введите логин"/>
                            </Form.Group>

                            <Form.Group className={styles.enterInput} controlId="password">
                                <Form.Label>Пароль</Form.Label>
                                <Form.Control type="password" placeholder="Введите пароль"/>
                            </Form.Group>

                            <Form.Group className={styles.button} controlId="submit-btn">
                                <Button type="submit">
                                    Войти
                                </Button>
                            </Form.Group>

                            <Form.Group className={styles.button} controlId="help-btns">
                                <Button onClick={gotoRegister}>
                                    Зарегистрироваться
                                </Button>
                            </Form.Group>

                            <Figure className="text-center">
                                <blockquote className="blockquote mb-4">
                                    <p className={styles.quote}>{quote.text}</p>
                                </blockquote>
                            </Figure>

                        </Form>
                    </Col>
                </Row>
            </Container>
        </>
    );
}