import {Quote, AlertMessage} from "../interfaces/interfaces";
import React, {FormEvent, useEffect, useState} from "react";
import { Button, Container, Form } from "react-bootstrap";
import styles from "../css/collection.module.css";
import QuoteList from "./QuoteList";
import { searchQuotes } from "../api/collection_api";

export default function Search(): JSX.Element {
    
    //alert part
    const [show, setShow] = useState<boolean>(false);
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [alertVariant, setAlertVariant] = useState<string>("");

    const enableAlert = (alert: AlertMessage): void => {
        if (alert !== undefined && alert.message !== undefined) {
            setAlertMsg(alert.message);
            setShow(true);
            alert.success ? setAlertVariant("success") : setAlertVariant("danger");
        }
    };
    //end of alert part
    
    const [quotes, setQuotes] = useState<Quote[]>([]);

    const [searchQuery, setSearchQuery] = useState<string>("");

    const onSubmit = async (event: FormEvent) => {
        event.preventDefault();
        searchQuotes(searchQuery).then(result =>{
            if(typeof result !== "string"){
                setQuotes(result);
            } else {
                const failAlert = {
                    success: false,
                    message: result as string,
                };
                enableAlert(failAlert);
            }
        });
    };

    const onFormChange = (event): void => {
        setSearchQuery(event.target.value);
    };

    return <Container className = {styles.mainContainer}>
        <h1>Поиск</h1>
        <Form onSubmit={onSubmit} onChange={onFormChange} className={styles.searchForm}>
            <Form.Group>
                <Form.Control placeholder="Искать здесь..." className={styles.searchField}/>
            </Form.Group>
        </Form>
        <QuoteList quotes={quotes}/>
    </Container>
}