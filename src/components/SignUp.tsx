import React, {FormEvent, useEffect, useState} from "react";
import {RegistrationRequest, AlertMessage} from "../interfaces/interfaces";
import {Button, Container, Form, Row, Alert} from "react-bootstrap";
import styles from "../css/signup.module.css"
import {useHistory} from "react-router";
import {register} from "../api/auth_api";

export default function SignUp(): JSX.Element {
    
    //alert part
    const [show, setShow] = useState<boolean>(false);
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [alertVariant, setAlertVariant] = useState<string>("");

    const enableAlert = (alert: AlertMessage): void => {
        if (alert !== undefined && alert.message !== undefined) {
            setAlertMsg(alert.message);
            setShow(true);
            alert.success ? setAlertVariant("success") : setAlertVariant("danger");
        }
    };
    //end of alert part
    
    const [credentials, setCredentials] = useState<RegistrationRequest>({
        login: "",
        email: "",
        password: ""
    });

    const [canSubmit, setCanSubmit] = useState<boolean>(false);

    const [passwordConfirmation, setPasswordConfirmation] = useState<string>("");

    const history = useHistory();

    useEffect(() => {
        setCanSubmit(
            credentials.login.length >= 4 &&
            credentials.email.length >= 4 &&
            credentials.password.length >= 4 &&
            credentials.password === passwordConfirmation
        );
    }, [credentials.email, credentials.login, credentials.password, passwordConfirmation]);

    const onFormChange = (event): void => {
        event.preventDefault();

        if (event.target.id === "passwordConfirmation") {
            setPasswordConfirmation(event.target.value);
        } else {
            setCredentials({...credentials, [event.target.id]: event.target.value});
        }
    };

    const onSubmit = (event: FormEvent): void => {
        event.preventDefault();
        register(credentials).then(result => {
            if (result === true) {
                history.push("/sign-in");
            } else {
                const failAlert = {
                    success: false,
                    message: result as string,
                };
                enableAlert(failAlert);
            }
        })
    }

    const getPasswordConfirmationClassName = () => {
        if (passwordConfirmation === credentials.password || credentials.password === "") {
            return "";
        } else {
            return "is-invalid";
        }
    };

    return <>
        <Alert
            variant={alertVariant}
            show={show}
            onClose={(): void => setShow(false)}
            dismissible 
        >
            <div className="p-1">{alertMsg}</div>
        </Alert>
        <Container className={styles.container}>
            <Row className={styles.formRow}>
                <Form className={`${styles.form} ${styles.inputColumn} p-4`} onChange={onFormChange}
                      onSubmit={onSubmit}>
                    <Form.Label className="h1 mb-4">Регистрация</Form.Label>

                    <Form.Group className={styles.regInput} controlId="login">
                        <Form.Label>Логин</Form.Label>
                        <Form.Control type="text"/>
                    </Form.Group>

                    <Form.Group className={styles.regInput} controlId="email">
                        <Form.Label>Почта</Form.Label>
                        <Form.Control type="email"/>
                    </Form.Group>

                    <Form.Group className={styles.regInput} controlId="password">
                        <Form.Label>Пароль</Form.Label>
                        <Form.Control type="password"/>
                    </Form.Group>

                    <Form.Group className={styles.regInput} controlId="passwordConfirmation">
                        <Form.Label>
                            Подтверждение пароля
                        </Form.Label>
                        <Form.Control type="password" className={getPasswordConfirmationClassName()}/>
                        <Form.Control.Feedback type="invalid">
                            Пароли не совпадают
                        </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group className={styles.regInput}>
                        <Button type="submit" className={`btn-success`} disabled={!canSubmit}>
                            Зарегистрироваться
                        </Button>
                    </Form.Group>
                </Form>
            </Row>
        </Container>
    </>
}